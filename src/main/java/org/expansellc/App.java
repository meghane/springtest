package org.expansellc;

import org.expansellc.service.CustomerService;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {

        System.out.println( "Hello World From Meghan's Spring Java Annotation Application!" );

        ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);
        CustomerService service = context.getBean("customerService", CustomerService.class);

       System.out.println( service.findAll().get(0).getFirstName()  );

    }
}
