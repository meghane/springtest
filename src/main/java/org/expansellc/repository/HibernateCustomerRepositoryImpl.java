package org.expansellc.repository;

import org.expansellc.model.Customer;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository("customerRepository")
public class HibernateCustomerRepositoryImpl implements CustomerRepository {

    public List<Customer> findAll() {
        System.out.println("Repository Instance is " + this.toString());
        List customers = new ArrayList();
        Customer c = new Customer();
        c.setFirstName("Meghan");
        c.setLastName("Erickson");
        customers.add(c);
        return customers;
    }
}
