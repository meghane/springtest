package org.expansellc;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan({"org.expansellc"})
public class AppConfig {

//    Replaced with @Service('customerService') in CustomerServiceImpl
//    @Bean(name = "customerService")
//    public CustomerService getCustomerService() {
//        CustomerServiceImpl service = new CustomerServiceImpl();
//        return service;
//    }

}
