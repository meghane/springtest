package org.expansellc.service;

import org.expansellc.model.Customer;

import java.util.List;

public interface CustomerService {
    List<Customer> findAll();
}
