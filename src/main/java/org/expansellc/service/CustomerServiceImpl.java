package org.expansellc.service;

import org.expansellc.model.Customer;
import org.expansellc.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("customerService")
@Scope(ConfigurableBeanFactory.SCOPE_SINGLETON)
public class CustomerServiceImpl implements CustomerService {

    private CustomerRepository customerRepository;

    @Autowired
    public void setCustomerRepository(CustomerRepository customerRepository) {
        System.out.println("setter injection autowired !");
        this.customerRepository = customerRepository;
    }

    public List<Customer> findAll() {
        System.out.println("Service Instance is " + this.toString());
        return customerRepository.findAll();
    }

}
