###SpringTest README ###
* A simple project to highlight differences between xml 
and java based Spring configurations

#### Spring Configurations Based Off XML and Java (Separately)####

####Branches####
* `git checkout xml`
* `git checkout java`
* `git checkout xml-autowired`

#### Notes ####
* Spring enabled enterprise development without an application server.
* Spring is completely POJO based - i.e. all code could be written without Spring.
* Spring puts focus on the business.
* Spring is WORA (Write Once, Run Anywhere).

##### XML Configuration #####
* can be simpler than java
* separation of concerns
* ` ApplicationContext context = new ClassPathXmlApplicationContext("applicationContext.xml");`

#### Java Configuration ####
* AppConfig.java replaces applicationContext.xml
* `@Configuration` annotation at class-level on `AppConfig.java` indicates 
`@Bean` methods will be declared and should be processed by the Spring container
* `ApplicationContext context = new AnnotationConfigApplicationContext(AppConfig.class);`
 
#### AutoWiring ####
 * `@ComponentScan({"org.explansellc"})` searches the package for `@Autowire` and `@Repository` annotations
 * Autowiring occurs at member variables, setters, constructors
 
##### 4 Types of Bean Autowiring #####
1. Type - property can be wired of only one bean of that type exists in the container
2. Name - allows multiples of same type
3. Constructor
4. No/None - cannot be autowired
 
#### Stereotypes ####
* 3 Main Annotations in Spring Core to help find Beans
* Component - any POJO
* Service - extends Component, contains biz layer
* Repository - extends Component, data layer
 
 
#### Prototype Design Pattern ####
 * Guarantees a unique instance per request
 
#### Properties ####
 * Java: PropertySourcesPlaceholderConfigurer makes properties from source available in the context

```
 @PropertySource("app.properties")
  public class AppConfig {
     
     @Bean
     public static PropertySourcesPlaceholderConfigurer getPropertySourcesPlaceholderConfigurer() {
         return new PropertySourcesPlaceholderConfigurer();
     }
  }
  
``` 
  
 * XML: 
```
 <context:property-placeholder location="app.properties"/>
 
  <bean name="customerRepository"
           class="org.expansellc.repository.HibernateCustomerRepositoryImpl">
         <property name="dbUsername" value="${db.username}"/>
     </bean>
``` 
 
 